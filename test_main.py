from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def get_test_token():
    data = {'grant_type': '', 'username': 'test', 'password': 'secret', 'scope': '', 'client_id': '',
            'client_secret': ''}
    test_token = client.post('/token',
                             headers={"accept": "application/json",
                                      'Content-Type': 'application/x-www-form-urlencoded'},
                             data=data)
    return test_token


def test_get_test_token():
    assert get_test_token()


def test_read_payment_unauthenticated():
    response = client.get("/users/me/payment/", headers={"accept": "application/json"})
    assert response.status_code == 401
    assert response.json() == {
        "detail": "Not authenticated",
    }


def test_read_user_unauthenticated():
    response = client.get("/users/me/", headers={"accept": "application/json"})
    assert response.status_code == 401
    assert response.json() == {
        "detail": "Not authenticated",
    }


def test_read_payment_authenticated():
    test_token = get_test_token().json()
    response = client.get('/users/me/payment/',
                          headers={"accept": "application/json",
                                   'Authorization': f'Bearer {test_token["access_token"]}'})
    assert response.status_code == 200
    assert response.json() == {
        "user_id": 1,
        "amount": 6000,
        "next_raise": "2025-05-04"
    }


def test_read_user_authenticated():
    test_token = get_test_token().json()
    response = client.get("/users/me/",
                          headers={"X-Token": "hailhydra", 'Authorization': f'Bearer {test_token["access_token"]}'})
    assert response.status_code == 200
    assert response.json() == {
        "username": "test",
        "email": 'testing',
        "full_name": 'testing row',
        "is_active": True
    }

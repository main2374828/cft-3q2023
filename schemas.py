import datetime

from pydantic import BaseModel

class Payment(BaseModel):
    user_id: int | None = None
    amount: float
    next_raise: datetime.date
    class ConfigDict:
        from_attributes = True



# Token and TokenData models for JWT
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


# User and UserInDB models for user data
class User(BaseModel):
    username: str
    email: str | None = None
    full_name: str | None = None
    is_active: bool | None = True
    class ConfigDict:
        from_attributes = True


class UserInDB(User):
    hashed_password: str
    id: int
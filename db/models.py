from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, Date
from sqlalchemy.orm import relationship

from .database import Base, engine


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String(40), unique=True)
    full_name = Column(String(200))
    email = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    payment_info = relationship("Payment", back_populates="user")

class Payment(Base):
    __tablename__ = "payments"

    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    amount = Column(Float)
    next_raise = Column(Date)
    user = relationship("User", back_populates="payment_info")

# Creating tables if needed
Base.metadata.create_all(bind=engine)

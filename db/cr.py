from sqlalchemy.orm import Session
from . import models
import schemas
from .database import SessionLocal


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_user_by_un(username: str, db: Session):
    return db.query(models.User).filter(models.User.username == username).first()


def create_user(user: schemas.User, password: str, db: Session):
    from dependencies import get_password_hash
    db_user = models.User(**user.model_dump(), hashed_password=get_password_hash(
        password))  # (email=user.email, hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_payment_info(username: str, db: Session):
    user_id = get_user_by_un(username, db)
    return db.query(models.Payment).filter(models.Payment.user_id == user_id).first()


def add_payment_info(payment_info: schemas.Payment, username: str, db: Session):
    user_id = get_user_by_un(username, db).id
    if not user_id:
        return None
    payment_info = payment_info.model_dump()
    payment_info.update({'user_id': user_id})
    db_payment = models.Payment(**payment_info)
    db.add(db_payment)
    db.commit()
    db.refresh(db_payment)
    return db_payment

from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, status
from db.cr import create_user, add_payment_info
from dependencies import get_current_active_user, db_session
from schemas import User, Payment

router = APIRouter(prefix='/users')
ENABLE_ADDING_USERS_AND_PAYMENTS = False

# Endpoint to get the current user's items
@router.get("/me/payment/", tags=['users'], response_model=Payment)
async def read_own_payment(
        current_user: Annotated[User, Depends(get_current_active_user)]
):

    info_in_db = current_user.payment_info
    if not info_in_db:
        raise HTTPException(
            status_code=status.HTTP_204_NO_CONTENT,
            detail="No payment info for the selected user.",
        )
    return info_in_db[0]


# Endpoint to get the current user's data
@router.get("/me/", response_model=User, tags=['users'])
async def read_users_me(
        current_user: Annotated[User, Depends(get_current_active_user)]
):
    return current_user


# Endpoints to add data to the db
@router.post("/", response_model=User, tags=['users'], include_in_schema=ENABLE_ADDING_USERS_AND_PAYMENTS)
async def add_user(
        new_user: Annotated[User, Depends()], password: str, db: db_session
):
    user_in_db = create_user(new_user, password, db)
    return user_in_db


@router.post("/payment/", tags=['users'], response_model=Payment, include_in_schema=ENABLE_ADDING_USERS_AND_PAYMENTS)
async def add_payment(
        payment_info: Annotated[Payment, Depends()], username: str, db: db_session
):
    payment_info_in_db = add_payment_info(payment_info, username, db)
    if not payment_info_in_db:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No user found")

    return payment_info_in_db
